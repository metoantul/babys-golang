package auth

import (
	"babys-server/pkg/auth"
	"babys-server/pkg/random"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(c echo.Context) error {
	var reqBody auth.User
	err := c.Bind(&reqBody)
	if err != nil {
		return c.JSON(500, "Unable to parse")
	}
	reqBody.Created = time.Now().Format("2006-01-02")
	err = auth.Validate(&reqBody)
	if err != nil {
		return c.JSON(400, err.Error())
	}
	res, err := auth.GetByEmail(reqBody.Email)
	if err != nil {
		return c.JSON(500, nil)
	}
	if res == nil && err == nil {
		// hash the password
		hashed, err := bcrypt.GenerateFromPassword([]byte(reqBody.Password), 8)
		if err != nil {
			return c.JSON(500, "Unable to hash password")
		}
		reqBody.Password = string(hashed)
		// save
		err = auth.SaveUser(&reqBody)
		if err != nil {
			return c.JSON(500, "Unable to save user")
		}
		return c.NoContent(201)
	}
	return c.JSON(409, "Email already registered")
}

func Login(c echo.Context) error {
	var loginData auth.LoginData
	err := c.Bind(&loginData)
	if err != nil {
		return c.JSON(500, "Unable to parse")
	}
	err = auth.LoginValidator(&loginData)
	if err != nil {
		return c.JSON(400, "Validation failed")
	}

	res, err := auth.GetByEmail(loginData.Email)
	if err != nil {
		return c.JSON(500, "Unable to fetch")
	}
	if res == nil && err == nil {
		return c.JSON(401, "Bad credentials")
	}
	err = bcrypt.CompareHashAndPassword([]byte(res.Password), []byte(loginData.Password))
	if err != nil {
		return c.JSON(401, "Bad credentials")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"uid":       res.ID,
		"firstname": res.FirstName,
		"lastname":  res.LastName,
	})

	tString, err := token.SignedString([]byte("key"))
	if err != nil {
		return c.JSON(500, "Unable to create jwt")
	}
	tCookie := new(http.Cookie)
	tCookie.Name = "token"
	tCookie.Value = tString
	tCookie.Path = "/"
	tCookie.HttpOnly = true
	c.SetCookie(tCookie)

	return c.JSON(200, "Login success")
}

func ValidateUser(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	uid := user.Claims.(jwt.MapClaims)["uid"].(float64)
	res, err := auth.GetById(uid)
	if err != nil {
		return c.JSON(400, "Unable to fetch")
	}
	return c.JSON(200, res)
}

func UpdateUser(c echo.Context) error {
	var u *auth.User
	err := c.Bind(&u)
	if err != nil {
		return c.JSON(500, "Unable to parse")
	}
	user := c.Get("user").(*jwt.Token)
	uid := user.Claims.(jwt.MapClaims)["uid"].(float64)
	_, err = auth.UpdateUser(uid, u)
	if err != nil {
		return c.JSON(500, "Unable to update")
	}
	return c.NoContent(200)
}

func ForgotPassword(c echo.Context) error {
	b, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(500, "Unable to read")
	}
	data := struct {
		Email string `json:"email"`
	}{}
	err = json.Unmarshal(b, &data)
	if err != nil {
		return c.JSON(500, "Unable to parse")
	}
	res, err := auth.GetByEmail(data.Email)
	if err != nil {
		return c.JSON(500, "Unable to fetch")
	}
	if res == nil && err == nil {
		return c.JSON(404, "Not found")
	}
	var hash = random.String(20)
	var uid = res.ID
	ra, err := auth.ForgotPassHashUpdate(uid, hash)
	if err != nil {
		return c.JSON(500, "Unable to update")
	}
	if ra == 0 {
		return c.JSON(500, "Not updated")
	}
	return c.JSON(200, hash)
}

func ResetPassword(c echo.Context) error {
	hash := c.Param("hash")
	user, err := auth.GetByHash(hash)
	if err != nil {
		return c.JSON(500, "Unable to fetch")
	}
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(500, "Unable to fetch payload")
	}
	data := struct {
		Password       string `json:"password"`
		RepeatPassword string `json:"repeat_password"`
	}{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return c.JSON(500, "Unable to fetch paylaod")
	}
	if data.Password != data.RepeatPassword {
		return c.JSON(400, "Passwords does not match")
	}
	hashed, err := bcrypt.GenerateFromPassword([]byte(data.Password), 8)
	if err != nil {
		return c.JSON(500, "Could not hash password")
	}
	newPwd := string(hashed)
	res, err := auth.UpdatePassword(newPwd, user.ID)
	if err != nil {
		return c.JSON(500, "Unable to update")
	}
	if res == 0 {
		return c.NoContent(404)
	}
	_, err = auth.ForgotPassHashUpdate(user.ID, "")
	if err != nil {
		return c.JSON(500, "Unable to update")
	}
	c.NoContent(200)
	return nil
}

func Logout(c echo.Context) error {
	tCookie := new(http.Cookie)
	tCookie.Name = "token"
	tCookie.Value = ""
	tCookie.Path = "/"
	tCookie.HttpOnly = true
	c.SetCookie(tCookie)
	return c.NoContent(200)
}
