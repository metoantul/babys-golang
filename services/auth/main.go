package main

import (
	auth "babys-server/services/auth/handlers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	app := echo.New()
	jwtmiddleware := middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey:  []byte("key"),
		TokenLookup: "cookie:token",
	})

	app.POST("/api/v1/auth/login", auth.Login)
	app.GET("/api/v1/auth/validate", auth.ValidateUser, jwtmiddleware)
	app.PUT("/api/v1/auth/update", auth.UpdateUser, jwtmiddleware)
	app.POST("/api/v1/auth/create-user", auth.CreateUser)
	app.POST("/api/v1/auth/forgot-password", auth.ForgotPassword)
	app.POST("/api/v1/auth/reset-password/:hash", auth.ResetPassword)
	app.POST("/api/v1/auth/logout", auth.Logout)

	app.Logger.Fatal(app.Start(":8080"))
}
