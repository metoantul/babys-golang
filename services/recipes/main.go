package main

import (
	recipes "babys-server/services/recipes/handlers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	app := echo.New()

	jwtmiddleware := middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey:  []byte("key"),
		TokenLookup: "cookie:token",
	})

	app.POST("/api/v1/recipes", recipes.Create, jwtmiddleware)
	app.PUT("/api/v1/recipes/me/:id", recipes.Update, jwtmiddleware)
	app.GET("/api/v1/recipes/me", recipes.GetMine, jwtmiddleware)
	app.DELETE("/api/v1/recipes/:id", recipes.Remove, jwtmiddleware)
	app.GET("/api/v1/recipes/category/:cat", recipes.GetByCategory)
	app.GET("/api/v1/recipes/:id", recipes.GetOne)

	app.Logger.Fatal(app.Start(":8081"))
}
