package recipes

import (
	"babys-server/pkg/recipes"
	"strconv"

	validator "github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

var validate *validator.Validate

func Create(c echo.Context) error {
	var reqBody recipes.Recipe
	err := c.Bind(&reqBody)
	if err != nil {
		return c.JSON(500, "Unable to parse")
	}

	// validate
	validate = validator.New()
	err = validate.Struct(reqBody)
	if err != nil {
		return c.JSON(400, "Validation failed")
	}

	// get uid
	uCheck := c.Get("user")
	if uCheck != nil {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		reqBody.Uid = int(claims["uid"].(float64))
		err = recipes.SaveRecipe(&reqBody)
		if err != nil {
			return c.JSON(500, "Unable to save")
		}
		return c.NoContent(201)
	}
	return nil
}

func GetMine(c echo.Context) error {
	uCheck := c.Get("user")
	if uCheck != nil {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		uid := int(claims["uid"].(float64))
		res, err := recipes.GetMine(uid)
		if err != nil {
			return c.JSON(500, "Unable to fetch")
		}
		return c.JSON(200, res)
	}
	return nil
}

func Remove(c echo.Context) error {
	uCheck := c.Get("user")
	if uCheck != nil {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		uid := int(claims["uid"].(float64))
		rid := c.Param("id")
		intVar, _ := strconv.Atoi(rid)
		res, err := recipes.RemoveRecipe(uid, intVar)
		if err != nil {
			return c.NoContent(500)
		}
		if res == 0 {
			return c.JSON(401, "no permisions")
		}
		c.NoContent(204)
	}
	return nil
}

func GetByCategory(c echo.Context) error {
	cat := c.Param("cat")
	res, err := recipes.GetCategory(cat)
	if err != nil {
		return c.NoContent(404)
	}
	return c.JSON(200, res)
}

func GetOne(c echo.Context) error {
	r := c.Param("id")
	res, err := recipes.GetOne(r)
	if err != nil {
		return c.NoContent(404)
	}
	c.JSON(200, res)
	return nil
}

func Update(c echo.Context) error {

	uCheck := c.Get("user")
	if uCheck != nil {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		uid := int(claims["uid"].(float64))
		// r, err := strconv.Atoi(c.Param("id"))
		// if err != nil {
		// 	return c.NoContent(400)
		// }
		var rec *recipes.Recipe
		err := c.Bind(&rec)
		if err != nil {
			return c.JSON(500, "err1")
		}
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return c.JSON(500, "err2") // unable to convert
		}
		res, err := recipes.Update(uid, id, rec)
		if err != nil {
			return c.JSON(500, "err3")
		}
		if res == 0 {
			return c.NoContent(401)
		}
		return c.JSON(200, "Updated")
	}
	return c.NoContent(401)
}
