package db

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var DBClient *sqlx.DB

const dsn = "root:meto@tcp(127.0.0.1:3306)/babys_golang?charset=utf8mb4&parseTime=True&loc=Local"

func DB() (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, fmt.Errorf("error happened while connecting to the DB")

	}
	fmt.Println("Successfully connected to the DB")
	DBClient = db
	return DBClient, nil
}
