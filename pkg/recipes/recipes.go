package recipes

import (
	"babys-server/pkg/db"
	"fmt"
	"log"
)

func SaveRecipe(r *Recipe) error {
	db, err := db.DB()
	if err != nil {
		return err
	}
	_, err = db.NamedExec(createRecipeSql, r)
	if err != nil {
		return nil
	}
	return nil
}

func GetMine(uid int) ([]Recipe, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	// args := map[string]interface{}{
	// 	"uid": uid,
	// }
	r := []Recipe{}
	err = db.Select(&r, `SELECT * FROM recipes WHERE uid = ?`, uid)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return r, nil
}

func RemoveRecipe(uid int, rid int) (int, error) {
	db, err := db.DB()
	if err != nil {
		return 0, err
	}
	args := map[string]interface{}{
		"uid": uid,
		"rid": rid,
	}
	r, err := db.PrepareNamed(deleteRecipeSql)
	// res, err := db.NamedExec(deleteRecipeSql, args)
	if err != nil {
		fmt.Println("here error happened")
		return 0, err
	}
	res, err := r.Exec(args)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	rowsAft, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return 0, err
	}
	if int(rowsAft) == 0 {
		return 0, nil
	}
	return int(rowsAft), nil
}

func GetCategory(c string) ([]Recipe, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	cat := []Recipe{}
	err = db.Select(&cat, "SELECT * FROM recipes WHERE category = ?", c)
	log.Println(cat)
	log.Println(c)
	if err != nil {
		return nil, err
	}
	return cat, nil
}

func GetOne(id string) (*Recipe, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	r := &Recipe{}
	err = db.Get(r, "SELECT * FROM recipes WHERE id = ?", id)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func GetOneMine(uid int, id int) (*Recipe, error) {
	db, err := db.DB()
	if err != nil {
		log.Println("here")
		return nil, err
	}
	r := &Recipe{}
	args := map[string]interface{}{
		"id":  id,
		"uid": uid,
	}
	log.Println(r)
	prep, err := db.PrepareNamed("SELECT * FROM recipes WHERE id = :id AND uid = :uid")
	// err = db.Get(r, "SELECT * FROM recipes WHERE id = :id AND uid = :uid", args)
	if err != nil {
		fmt.Println("from here")
		return nil, err
	}
	err = prep.Get(r, args)
	log.Println(r)
	// if err.Error() == "sql: no rows in result set" {
	// 	return nil, nil
	// }
	if err != nil {
		return nil, err
	}
	return r, nil
}

func Update(uid int, id int, r *Recipe) (int, error) {
	db, err := db.DB()
	if err != nil {
		log.Println(err)
		return 0, err
	}
	args := map[string]interface{}{
		"title":     r.Title,
		"category":  r.Category,
		"prep_time": r.PrepTime,
		"short_des": r.ShortDes,
		"content":   r.Content,
		"id":        id,
		"uid":       uid,
	}
	res, err := db.NamedExec(updateRecipeSql, args)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	log.Println(err)
	ra, _ := res.RowsAffected()
	if ra == 0 {
		return 0, nil
	}
	return int(ra), nil
}

const createRecipeSql = `
	INSERT INTO recipes (title,category,prep_time,short_des,content,uid)
	VALUES (:title,:category,:prep_time,:short_des,:content,:uid)
`
const deleteRecipeSql = `
	DELETE FROM recipes
	WHERE id = :rid AND uid = :uid
`
const getCategorySql = `
	SELECT * FROM recipes
	WHERE category = brunch
`
const updateRecipeSql = `
	UPDATE recipes
	SET 
		title = IF(:title is NULL OR :title = '', title,:title),
		category = IF(:category is NULL OR :category = '', category,:category),
		prep_time = IF(:prep_time is NULL OR :prep_time = '', prep_time,:prep_time),
		short_des = IF(:short_des is NULL OR :short_des = '', short_des,:short_des),
		content = IF(:content is NULL OR :content = '', content,:content)
	WHERE id = :id AND uid = :uid
`
