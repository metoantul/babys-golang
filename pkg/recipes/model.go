package recipes

type Recipe struct {
	ID       int    `json:"id" db:"id"`
	Title    string `json:"title" db:"title"`
	Category string `json:"category" db:"category"`
	PrepTime int    `json:"prep_time" db:"prep_time"`
	ShortDes string `json:"short_des" db:"short_des"`
	Content  string `json:"content" db:"content"`
	Uid      int    `json:"uid" db:"uid"`
}
