package auth

import (
	"babys-server/pkg/db"
	"fmt"
	"log"
)

func SaveUser(u *User) error {
	db, err := db.DB()
	if err != nil {
		return err
	}
	_, err = db.NamedExec(createUserSql, u)
	if err != nil {
		return err
	}
	return nil
}

func GetByEmail(email string) (*User, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	u := User{}
	err = db.Get(&u, getByEmailSql, email)
	// todo refactoring
	if err != nil && err.Error() != "sql: no rows in result set" {
		return nil, err
	}
	if len(u.Email) == 0 {
		return nil, nil
	}
	return &u, nil
}

func GetById(id float64) (*User, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	u := User{}
	err = db.Get(&u, getByIdSql, id)
	if err != nil {
		return nil, err
	}
	log.Println(u)
	// if u.ID == 0 {
	// 	return nil, err
	// }
	return &u, nil
}

func GetAllUsers() ([]User, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	users := []User{}
	err = db.Select(&users, getAllUsersSql)
	if err != nil {
		return nil, err
	}
	return users, nil
}
func UpdateUser(uid float64, u *User) (int, error) {
	db, err := db.DB()
	if err != nil {
		return 0, err
	}
	args := map[string]interface{}{
		"id":        uid,
		"firstname": u.FirstName,
		"lastname":  u.LastName,
	}
	// log.Println(args)
	res, err := db.NamedExec(updateUserSql, args)
	if err != nil {
		fmt.Println(err.Error())

		return 0, err
	}
	rowsAff, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowsAff), nil
}

func ForgotPassHashUpdate(uid int, hash string) (int, error) {
	db, err := db.DB()
	if err != nil {
		return 0, err
	}
	args := map[string]interface{}{
		"id":               uid,
		"forgot_pass_hash": hash,
	}
	log.Println(args)
	res, err := db.NamedExec(updateForgotPassHashSql, args)
	if err != nil {
		return 0, err
	}
	rowsAff, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowsAff), nil
}

func GetByHash(hash string) (*User, error) {
	db, err := db.DB()
	if err != nil {
		return nil, err
	}
	u := User{}
	err = db.Get(&u, `SELECT * FROM users WHERE forgot_pass_hash = ?`, hash)
	if err != nil {
		fmt.Println("here1")
		return nil, err
	}
	if u.ID == 0 {
		fmt.Println("here1")

		return nil, fmt.Errorf("not found")
	}
	return &u, nil
}

func UpdatePassword(pwd string, uid int) (int, error) {
	db, err := db.DB()
	if err != nil {
		return 0, err
	}
	args := map[string]interface{}{
		"id":       uid,
		"password": pwd,
	}
	res, err := db.NamedExec(`UPDATE users SET password = :password WHERE id = :id`, args)
	if err != nil {
		return 0, err
	}
	rowsAff, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowsAff), nil
}

const createUserSql = `
	INSERT INTO users (firstName,lastname,email,password,dob,created,forgot_pass_hash) 
	VALUES (:firstname,:lastname,:email,:password,:dob,:created,:forgot_pass_hash)
`
const getByEmailSql = `
	SELECT * 
	FROM users
	WHERE email = ?
`
const getByIdSql = `
	SELECT id,firstname,lastname,email,dob,created
	FROM users
	WHERE id = ?
`
const getAllUsersSql = `
	SELECT * 
	FROM users
`
const updateUserSql = `
	UPDATE users 
	SET 
		firstname = IF(:firstname IS NULL OR :firstname = '', firstname,:firstname), 
		lastname = IF(:lastname is NULL OR :lastname = '',lastname,:lastname) 
	WHERE id = :id
`
const updateForgotPassHashSql = `
	UPDATE users 
	SET 
		forgot_pass_hash = :forgot_pass_hash 
	WHERE id = :id
`
