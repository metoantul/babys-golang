package auth

type User struct {
	ID             int    `json:"id" db:"id"`
	FirstName      string `json:"first_name" db:"firstname" validate:"required"`
	LastName       string `json:"last_name" db:"lastname" validate:"required"`
	Email          string `json:"email" db:"email" validate:"required,email"`
	Password       string `json:"password" db:"password" valdiate:"required"`
	Dob            string `json:"dob" db:"dob" validate:"required"`
	Created        string `json:"created" db:"created"`
	ForgotPassHash string `json:"forgot_pass_hash,omitempty" db:"forgot_pass_hash,omitempty"`
}
