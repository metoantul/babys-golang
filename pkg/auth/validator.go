package auth

import validator "github.com/go-playground/validator/v10"

var validate *validator.Validate

type LoginData struct {
	Email    string `json:"email" db:"email"`
	Password string `json:"password" db:"password"`
}

func Validate(u *User) error {
	validate = validator.New()
	return validate.Struct(u)
}

func LoginValidator(l *LoginData) error {
	validate = validator.New()
	return validate.Struct(l)
}

// todo remove this file and refactor pkg
